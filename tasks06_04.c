#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

clock_t begin, end;

void FillArr(int* Arr, int SIZE)
{
	int i = 0;
	for (i = 0; i < SIZE; i++)
	{
		Arr[i] = rand();
	}
}

void PrintArr(int* Arr, int SIZE)
{
	int i = 0;
	printf("\n");
	for (i = 0; i < SIZE; i++)
		printf("%d ", Arr[i]);
	printf("\n");
}

int SumConventional(int* Arr, int SIZE)
{
	int i = 0;
	int sum = 0;
	for (i = 0; i < SIZE; i++)
		sum += Arr[i];
	return sum;
}

int SumRecursive(int* Arr, int SIZE)
{
	if (SIZE == 0)
	{
		return 0;
	}
	else
	{
		return(Arr[SIZE] + SumRecursive(Arr, --SIZE));
	}
}

int main()
{
	int N=0;
	int M = 0;
	double t_c, t_r = 0;
	srand(time(0));

	puts("Please enter a number");
	scanf_s("%d", &M, sizeof(int));

	N = pow(2,M);
	int *BigArr = (int*)malloc(N*sizeof(int));
	
	FillArr(BigArr,N);
	PrintArr(BigArr, N);

	begin = clock();
	SumConventional(BigArr, N);
	end = clock();
	t_c = (double)(end - begin) / CLOCKS_PER_SEC;

	begin = clock();
	SumRecursive(BigArr, N);
	end = clock();
	t_r = (double)(end - begin) / CLOCKS_PER_SEC;

	printf("Sum is: %d\ntime: %f\n", SumConventional(BigArr, N), t_c);
	printf("Sum is: %d\ntime: %f\n", SumRecursive(BigArr, N),t_r);

	return 0;
}