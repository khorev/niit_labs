#include <stdio.h>

float euro;
int inch, ft;

int main()
{
	do
	{
		printf("Please enter size in American system\nLike this: 1ft 5inch\n");
		scanf_s("%dft %dinch", &ft, &inch, sizeof(ft), sizeof(inch));
	} while (inch>11);
	
	euro = (ft * 12 + inch)*2.54;
	printf("This size in European system is equal to %.1f\n", euro);
	
	return 0;
};