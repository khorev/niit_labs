#include <stdio.h>

char arr[256][256] = {0};

void Shuffle(char* WORD)
{
	int j = 256;
	char tmp;
	//if small word
	int CharCount = 0;
	//else if more the 3 symbols
	if (strlen(WORD)>3)
		//-2 because first and last should be fixed 
		CharCount = strlen(WORD) - 2;
	while (CharCount)
	{
		//start from 1 instead 0
		j = (rand() % CharCount) + 1;
		tmp = WORD[j];
		WORD[j] = WORD[CharCount];
		WORD[CharCount] = tmp;
		CharCount--;
	}
}

char* DivideIntoWords(char * line)
{
	int i = 0;
	int LineCounter = 0;
	int TmpCounter = 0;
	char tmpWord[256] = { 0 };
	char ReturnLine[256] = { 0 };
	while (*line)
	{
		//if not space then make temporary string
		if (*line != ' ' && *line != '\n')
		{
			tmpWord[i] = *line;
		}
		//if space
		else if (*line == ' ' || *line == '\n')
		{
			//if temporary word exist
			if (strlen(tmpWord))
			{
				//shuffle it and write to return line with a space at the end
				Shuffle(tmpWord);
				TmpCounter = 0;
				while (tmpWord[TmpCounter])
				{
					ReturnLine[LineCounter + TmpCounter] = tmpWord[TmpCounter];
					tmpWord[TmpCounter] = '\0';
					TmpCounter++;
				}
				LineCounter = LineCounter + TmpCounter + 1;
				ReturnLine[LineCounter - 1] = ' ';
				i = -1;
			}
			//if temporary word doesn't exist then simply write a space at the end of the return string
			else
			{
				ReturnLine[LineCounter++] = ' ';
				i = -1;
			}
		}
		i++;
		line++;
	}
	//if end of line is a character, not '\n' or ' '
	if (strlen(tmpWord))
	{
		Shuffle(tmpWord);
		TmpCounter = 0;
		while (tmpWord[TmpCounter])
		{
			ReturnLine[LineCounter + TmpCounter] = tmpWord[TmpCounter];
			tmpWord[TmpCounter] = '\0';
			TmpCounter++;
		}
	}
	ReturnLine[strlen(ReturnLine)] = '\n';
	return ReturnLine;
}

int main()
{
	FILE *in, *out;
	
	int i = 0;
	fopen_s(&in, "in.txt", "rt");
	fopen_s(&out, "out.txt", "wt");
	srand(time(0));
	if ((out == NULL) || (in == NULL))
	{
		perror("File error!");
		return 1;
	}

	while (fgets(arr[i], 256, in))
	{
		fprintf_s(out, "%s", DivideIntoWords(arr[i]));
		i++;
	}
	fclose(in);
	fclose(out);
	return 0;
}