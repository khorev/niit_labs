#include <stdio.h>
#include <time.h>

int main()
{
	int table[128] = { 0 };
	char str[256];
	int i = 0;

	puts("Enter a string, please");
	fgets(str, 256, stdin);

	for (i = 0; i < (int)strlen(str); i++)
	{
		table[str[i]]++;
	}

	for (i = 32; i < 128; i++)
	{
		if (table[i]>0)
		{
			printf("%c %d\n", (char)i, table[i]);
		}
	}

	return 0;
}