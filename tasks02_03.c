#include <stdio.h>
#include <time.h>

int main()
{
	int count, i, j, k;

	printf("Enter number of lines, please: ");
	scanf_s("%d", &count, sizeof(count));
	for (i = 1; i <= count; i++)
	{
		for (k = count; k > i - 1; k--)
		{
			printf(" ");
		}

		for (j = 1; j <= 2 * i - 1; j++)
		{
			printf("*");
		}
		printf("\n");
	}
	for (i = count - 1; i >= 1; i--)
	{
		for (k = i - 1; k < count; k++)
		{
			printf(" ");
		}

		for (j = 2 * i - 1; j >= 1; j--)
		{
			printf("*");
		}
		printf("\n");
	}

	return 0;
}