#include <stdio.h>
#include <time.h>

#define N 50
#define M 50

char arr[N][M+1] = {0};
char choice[] = "    *";

void Clear()
{
	int i, j;
	for (i = 0; i<N; i++)
		for (j = 0; j < M; j++)
			arr[i][j] = 0;
}

void Fill()
{
	int i, j;
	for (i = 0; i < N/2; i++)
		for (j = 0; j < M/2; j++)
			arr[i][j] = choice[rand() % 5];
}

void Copy()
{
	int i, j,k;

	//filling left lower quadrant
	k = N / 2-1;
	for (i = N / 2; i < N; i++)
	{
		for (j = 0; j < M / 2; j++)
			arr[i][j] = arr[k][j];
		k--;
	}

	//filling right side

	for (i = 0; i < N; i++)
	{
		k = M / 2 - 1;
		for (j = M / 2; j < M; j++)
			arr[i][j] = arr[i][k--];
	}

}

void PrintArr()
{
	int i;
	for (i = 0; i < N; i++)
		printf("%s\n", arr + i);
}

int main()
{
	while (1)
	{
		srand(time(0));
		Fill();
		Copy();
		PrintArr();
		Clear();
		clock_t begin = clock();
		while (clock() < begin + 1000);
		system("cls");
	}
	return 0;
}