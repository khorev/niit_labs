#include <stdio.h>

void swap(char *begin, char *end)
{
	while (*end)
	{
		*begin++ = *end++;
	}
	*begin = 0;

};

int main()
{
	char str[256];
	int i = 0;
	int j = 0;

	puts("Enter a string, please\n");
	fgets(str, 256, stdin);
	if (str[strlen(str) - 1] == '\n')
		str[strlen(str) - 1] = 0;

	while (str[0] == ' ')
	{
		if (str[0] == ' ')
			swap(&str[0], &str[1]);
	}

	for (j = 0; j < strlen(str); j++)
	{
		for (i = 0; i < strlen(str); i++)
		{
			if (str[i] == ' ' && str[i + 1] == ' ')
				swap(&str[i], &str[i + 1]);
		}
	}

	printf("String without extra spaces:\n%s\n", str);
	return 0;
}