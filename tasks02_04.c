#include <stdio.h>
#include <time.h>

int main()
{
	char arr[] = "6gg0g7";
	printf("Original array: %s\n", arr);
	int i, j;
	char temp;
	int delimeter;
	int flag = 0;
	int count = 0;
	delimeter = strlen(arr);

	for (i = 0; i <delimeter - count - 1; i++)
	{
		if (arr[i] >= '0' && arr[i] <= '9')
		{
			for (j = strlen(arr); j >0, j--;)
			{
				if ((arr[j] >= 'A' && arr[j] <= 'Z') || (arr[j] >= 'a' && arr[j] <= 'z'))
				{
					temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
					if (flag == 0)
						delimeter = j;
					flag = 1;
				}
			}
			count++;
		}
	}
	printf("Groupped array: %s\n", arr);

	return 0;
}