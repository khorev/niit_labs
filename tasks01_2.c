#include <stdio.h>

int hours, minutes, seconds;

int main()
{
	do
	{
		printf("Please type time in format 'hh:mm:ss'\n");
		scanf_s("%d:%d:%d", &hours, &minutes, &seconds, sizeof(hours), sizeof(minutes), sizeof(seconds));
		printf("You enter %02d:%02d:%02d\n", hours, minutes, seconds);
	} while (hours > 23 || minutes > 59 || seconds > 59);

	if (hours >= 6 && hours <= 11)
		printf("Good morning!\n");
	if (hours >= 12 && hours <= 17)
		printf("Good day!\n");
	if (hours >= 18 && hours <= 23)
		printf("Good evening!\n");
	if (hours >= 0 && hours <= 5)
		printf("Good night!\n");
	
	return 0;
};