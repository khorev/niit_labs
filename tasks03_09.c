#include <stdio.h>
#include <time.h>

int main()
{
	char str[256];
	int i = 0;
	int templength=0;
	int maxlength=0;
	char tempsymbol;
	char maxsymbol;
	
	puts("Enter a string, please");
	fgets(str, 256, stdin);

	while (str[i])
	{
		if (str[i] == str[i + 1])
		{
			tempsymbol = str[i];
			templength++;
		}
		else
			templength = 0;
		if (templength > maxlength)
		{
			maxlength = templength;
			maxsymbol = tempsymbol;
		}
		i++;
	}
	printf("Longest chain is %d symbols of '%c'\n", maxlength+1, maxsymbol);

	return 0;
}