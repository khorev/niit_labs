#include <stdio.h>
char NumChar[10];
void convert(int Num)
{
	int cnt = 0;
	int TmpNum = Num;
	while (TmpNum)
	{
		TmpNum /= 10;
		cnt++;
	}
	if (Num / 10 == 0)
	{
		NumChar[0] = Num+48;
	}
	else
	{
		NumChar[cnt - 1] = (Num % 10)+48;
		convert(Num / 10);
	}
}

int main()
{
	int Num=0;
	puts("Please enter a number");
	scanf_s("%d", &Num, sizeof(long));
	convert(Num);
	printf("This is string: %s\n", NumChar);
	return 0;
}