#include <stdio.h>

void swap(char *begin, char *end)
{
	while (*end)
	{
		*begin++ = *end++;
	}
	*begin = 0;

};

int main()
{
	char str[256];
	int i = 0;
	int j = 0;
	int inWord = 0;
	int count = 0;
	int number = 0;

	puts("Enter a string, please:");
	fgets(str, 256, stdin);

	if (str[strlen(str) - 1] == '\n')
		str[strlen(str) - 1] = 0;

	while (str[i])
	{
		if (str[i] != ' ' && inWord == 0)
		{
			count++;
			inWord = 1;
		}
		else if (str[i] == ' ' && inWord == 1)
			inWord = 0;
		i++;
	}

	printf("Words number: %d\n", count);
	
	while (str[0] == ' ')
	{
		if (str[0] == ' ')
			swap(&str[0], &str[1]);
	}

	for (j = 0; j < strlen(str); j++)
	{
		for (i = 0; i < strlen(str); i++)
		{
			if (str[i] == ' ' && str[i + 1] == ' ')
				swap(&str[i], &str[i + 1]);
		}
	}

	do
	{
		puts("Which word do you want to see(number)?");
		scanf_s("%d", &number, sizeof(number));
		if (number > count || number<1)
			puts("Error, this word doesn't exist");
	} while (number>count);
	
	str[strlen(str)] = ' ';

	i = 0;
	while (str[i])
	{
		if (str[i] != ' ')
		{
			if (!(number-1))
			printf("%c", str[i]);
		}
		else
		{
			number--;
		}
		i++;
	}
	printf("\n");
	return 0;
}