#include <stdio.h>

char arr[256][256] = { 0 };
char* PWords[256] = { 0 };

void Shuffle(int SIZE,char** WORDS)
{
	int rnd=0;
	char* tmp;
	SIZE = SIZE + 1;
	while (SIZE)
	{
		rnd = (rand() % SIZE);
		tmp = WORDS[rnd];
		WORDS[rnd] = WORDS[SIZE-1];
		WORDS[SIZE-1] = tmp;
		SIZE--;
	}
}

int DivideIntoWords(char * line)
{
	int i = 0;
	int j= 0;
	int InWord = 0;
	int WordsCount = 0;
	char Words[256][256] = {0};
	//if no '\n' at the end of line we add it
	if (line[strlen(line) - 1] != '\n')
		line[strlen(line)] = '\n';
	while (*line)
	{
		//change '/n' to ' ' at the end of the line
		if (*line == '\n')
			*line = ' ';
		//write in array word
		if (*line != ' ')
		{
			Words[i][j++] = *line;
			InWord = 1;
		}
		//write all spaces after word
		if (*line == ' ' && InWord == 1)
		{
			Words[i][j++] = *line;
			if (*(line+1)!=' ')
				InWord = 2;
		}
		//jump on a next word and nulled flag and counter
		if (InWord == 2)
		{
			InWord = 0;
			i++;
			j = 0;
			WordsCount++;
		}
		line++;
	}
	for (i = 0; i < WordsCount+1; i++)
	{
		//filling variable from local variable
		PWords[i] = Words[i];
	}
	//return number of words
	return WordsCount;
}


int main()
{
	FILE *in, *out;
	
	int i = 0;
	int Num = 0;
	fopen_s(&in, "in.txt", "rt");
	fopen_s(&out, "out.txt", "wt");
	srand(time(0));
	if ((out == NULL) || (in == NULL))
	{
		perror("File error!");
		return 1;
	}

	while (fgets(arr[i], 256, in))
	{
		Num = DivideIntoWords(arr[i]);
		Shuffle(Num,PWords);
		for (i = 0; i <Num+1; i++)
			fprintf_s(out, "%s",PWords[i]);
		fprintf_s(out, "\n");
		i++;
	}
	fclose(in);
	fclose(out);
	return 0;
}